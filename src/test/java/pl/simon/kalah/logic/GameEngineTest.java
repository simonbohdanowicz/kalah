package pl.simon.kalah.logic;

import org.junit.Before;
import org.junit.Test;
import pl.simon.kalah.GameUtil;
import pl.simon.kalah.model.Game;

import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Szymon on 2017-06-26.
 */
public class GameEngineTest extends GameUtil{

    private GameEngine gameEngine;

    @Before
    public void setUp() {
        gameEngine = new GameEngine();
    }

    @Test
    public void moveStones_incrementNormal() throws Exception {
        //given
        Game initailStateGame = getStartGame();
        //when

        gameEngine.makeAMove(initailStateGame, 3, PLAYER_1);
        //then
        assertThat(initailStateGame.getPlayerSide(PLAYER_1).getOrdinaryPits()).isEqualTo(new int[]{6, 6, 6, 0, 7, 7});
        assertThat(initailStateGame.getPlayerSide(PLAYER_1).getKalah()).isEqualTo(1);
        assertThat(initailStateGame.getPlayerSide(PLAYER_2).getOrdinaryPits()).isEqualTo(new int[]{7, 7, 7, 6, 6, 6});
        assertThat(initailStateGame.getPlayerSide(PLAYER_2).getKalah()).isEqualTo(0);
        assertThat(initailStateGame.getLastActionSideOwner()).isEqualTo(PLAYER_2);
        assertThat(initailStateGame.getLastActionPitNumber()).isEqualTo(2);
        assertThat(initailStateGame.getMovingPlayer()).isEqualTo(PLAYER_2);
    }

    @Test
    public void moveStones_incrementAndHaveAnotherMove() throws Exception {
        //given
        Game testGame = getStartGame();
        testGame.getPlayerSide(PLAYER_1).emptyPit(3);
        testGame.getPlayerSide(PLAYER_1).addStonesToPit(3, 3);
        //when
        gameEngine.makeAMove(testGame, 3, PLAYER_1);
        //then
        assertThat(testGame.getPlayerSide(PLAYER_1).getOrdinaryPits()).isEqualTo(new int[]{6, 6, 6, 0, 7, 7});
        assertThat(testGame.getPlayerSide(PLAYER_1).getKalah()).isEqualTo(1);
        assertThat(testGame.getPlayerSide(PLAYER_2).getOrdinaryPits()).isEqualTo(new int[]{6, 6, 6, 6, 6, 6});
        assertThat(testGame.getPlayerSide(PLAYER_2).getKalah()).isEqualTo(0);
        assertThat(testGame.getLastActionSideOwner()).isEqualTo(PLAYER_1);
        assertThat(testGame.getLastActionPitNumber()).isEqualTo(6);
        assertThat(testGame.getMovingPlayer()).isEqualTo(PLAYER_1);
    }

    @Test
    public void moveStones_incrementAndAttack() throws Exception {
        //given
        Game testGame = getStartGame();
        testGame.getPlayerSide(PLAYER_1).emptyPit(0);
        testGame.getPlayerSide(PLAYER_1).addStonesToPit(0, 3);
        testGame.getPlayerSide(PLAYER_1).emptyPit(3);
        //when
        gameEngine.makeAMove(testGame, 0, PLAYER_1);
        //then
        assertThat(testGame.getPlayerSide(PLAYER_1).getOrdinaryPits()).isEqualTo(new int[]{0, 7, 7, 0, 6, 6});
        assertThat(testGame.getPlayerSide(PLAYER_1).getKalah()).isEqualTo(7);
        assertThat(testGame.getPlayerSide(PLAYER_2).getOrdinaryPits()).isEqualTo(new int[]{6, 6, 0, 6, 6, 6});
        assertThat(testGame.getPlayerSide(PLAYER_2).getKalah()).isEqualTo(0);
        assertThat(testGame.getLastActionSideOwner()).isEqualTo(PLAYER_1);
        assertThat(testGame.getLastActionPitNumber()).isEqualTo(3);
        assertThat(testGame.getMovingPlayer()).isEqualTo(PLAYER_2);
    }

    @Test
    public void moveStones_incrementAndOmitOtherPlayerKalah() throws Exception {
        //given
        Game testGame = getStartGame();
        testGame.getPlayerSide(PLAYER_1).addStonesToPit(3, 4);
        //when
        gameEngine.makeAMove(testGame, 3, PLAYER_1);
        //then
        assertThat(testGame.getPlayerSide(PLAYER_1).getOrdinaryPits()).isEqualTo(new int[]{7, 6, 6, 0, 7, 7});
        assertThat(testGame.getPlayerSide(PLAYER_1).getKalah()).isEqualTo(1);
        assertThat(testGame.getPlayerSide(PLAYER_2).getOrdinaryPits()).isEqualTo(new int[]{7, 7, 7, 7, 7, 7});
        assertThat(testGame.getPlayerSide(PLAYER_2).getKalah()).isEqualTo(0);
        assertThat(testGame.getLastActionSideOwner()).isEqualTo(PLAYER_1);
        assertThat(testGame.getLastActionPitNumber()).isEqualTo(0);
        assertThat(testGame.getMovingPlayer()).isEqualTo(PLAYER_2);
    }


    @Test
    public void isAttackIsPossible() {
        //given
        String movingPlayer = PLAYER_1;
        String sideOwner = PLAYER_1;
        int lastPitNumber = 4;
        int lastPitValue = 1;

        //when
        assertThat(gameEngine.isAttackIsPossible(sideOwner, lastPitNumber, lastPitValue, movingPlayer)).isTrue();
    }

    @Test
    public void isGameFinished_itIsNot() throws Exception {
        assertThat(gameEngine.isGameFinished(getStartGame())).isFalse();
    }

    @Test
    public void isGameFinished_playersHaveEmptyPits() throws Exception {
        Game testGame = getStartGame();
        IntStream.range(0, Game.KALAH_PIT_NUMBER).forEach(pitNumber ->
                testGame.getPlayers().forEach(player -> testGame.getPlayerSide(player).emptyPit(pitNumber)));
        //when -> then
        assertThat(gameEngine.isGameFinished(testGame)).isTrue();
    }

    @Test
    public void isGameFinished_onePlayersHasEmptyPits() throws Exception {
        Game testGame = getStartGame();
        String player = testGame.getPlayers().stream().findAny().get();
        IntStream.range(0, Game.KALAH_PIT_NUMBER).forEach(pitNumber ->
                testGame.getPlayerSide(player).emptyPit(pitNumber));
        //when -> then
        assertThat(gameEngine.isGameFinished(testGame)).isFalse();
    }

    @Test
    public void getOtherPlayer() throws Exception {
        //given
        Game testGame = getStartGame();
        //when
        String otherPlayer = gameEngine.getOtherPlayer(testGame, PLAYER_1);
        //them
        assertThat(otherPlayer).isEqualTo(PLAYER_2);
    }

    @Test
    public void checkIfGameHasEnded_onePlayerHasPitsEmpty() throws Exception {
        //give
        Game testGame = getStartGame();
        IntStream.range(0, Game.KALAH_PIT_NUMBER).forEach(pitNumber ->
                testGame.getPlayerSide(PLAYER_1).emptyPit(pitNumber));
        //when
        gameEngine.checkIfGameHasEnded(testGame);
        //then
        assertThat(testGame.getPlayerSide(PLAYER_2).getOrdinaryPits()).isEqualTo(new int[]{0, 0, 0, 0, 0, 0});
        assertThat(testGame.getPlayerSide(PLAYER_2).getKalah()).isEqualTo(36);
    }

    @Test
    public void checkIfGameHasEnded_noneOfPlayersHasPitsEmpty() throws Exception {
        //give
        Game testGame = getStartGame();
        //when
        gameEngine.checkIfGameHasEnded(testGame);
        //then
        assertThat(testGame.getPlayerSide(PLAYER_1).getOrdinaryPits()).isEqualTo(new int[]{6, 6, 6, 6, 6, 6});
        assertThat(testGame.getPlayerSide(PLAYER_1).getKalah()).isEqualTo(0);
        assertThat(testGame.getPlayerSide(PLAYER_2).getOrdinaryPits()).isEqualTo(new int[]{6, 6, 6, 6, 6, 6});
        assertThat(testGame.getPlayerSide(PLAYER_2).getKalah()).isEqualTo(0);
    }

    @Test
    public void isThereNewMovingPlayer() throws Exception {
        assertThat(gameEngine.isThereNewMovingPlayer(PLAYER_1, 6, PLAYER_1)).isFalse();
        assertThat(gameEngine.isThereNewMovingPlayer(PLAYER_1, 5, PLAYER_1)).isTrue();
        assertThat(gameEngine.isThereNewMovingPlayer(PLAYER_1, 5, PLAYER_2)).isTrue();
    }

}