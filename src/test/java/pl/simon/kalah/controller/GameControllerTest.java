package pl.simon.kalah.controller;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import pl.simon.kalah.GameUtil;
import pl.simon.kalah.KalahApplication;
import pl.simon.kalah.data.GameRepository;
import pl.simon.kalah.model.Game;
import pl.simon.kalah.util.GameUtils;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * Created by Szymon on 2017-06-28.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = KalahApplication.class)
@AutoConfigureMockMvc
public class GameControllerTest extends GameUtil{

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private MockMvc mvc;

    @Before
    public void setUp(){
        gameRepository.deleteAll();
    }

    @Test
    public void initNewGame() throws Exception {
        mvc.perform(post("/kalah")
                .param("game-name", GAME_NAME)
                .param("player1", PLAYER_1)
                .param("player2", PLAYER_2)
                .param("stone-number", String.valueOf(6))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.stoneNumber", Matchers.is(6)))
                .andExpect(jsonPath("$.name", Matchers.is(GAME_NAME)))
                .andExpect(jsonPath("$.players", Matchers.containsInAnyOrder(PLAYER_1, PLAYER_2)));
    }

    @Test
    public void initGame_withAlradyExistingName() throws Exception {
        gameRepository.save(getStartGame());

        mvc.perform(post("/kalah")
                .param("game-name", GAME_NAME)
                .param("player1", PLAYER_1)
                .param("player2", PLAYER_2)
                .param("stone-number", String.valueOf(6))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getGame() throws Exception {
        gameRepository.save(getStartGame());

        mvc.perform(get("/kalah/"+GAME_NAME)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.stoneNumber", Matchers.is(6)))
                .andExpect(jsonPath("$.name", Matchers.is(GAME_NAME)))
                .andExpect(jsonPath("$.players", Matchers.containsInAnyOrder(PLAYER_1, PLAYER_2)));
    }

    @Test
    public void makeAMove() throws Exception {
        Game game = getStartGame();
        game.setMovingPlayer(PLAYER_1);
        gameRepository.save(game);

        mvc.perform(put("/kalah/"+GAME_NAME+"/move")
                .param("player", PLAYER_1)
                .param("pit-number", String.valueOf(4))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }
}
