package pl.simon.kalah.util;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import pl.simon.kalah.GameUtil;
import pl.simon.kalah.model.Game;
import pl.simon.kalah.model.GameSide;

import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created by Szymon on 2017-06-28.
 */
public class GameUtilsTest extends GameUtil{

    @Test
    public void areAllPitsEmpty_pitsAreEmpty() throws Exception {
        //given
        Game testGame = getStartGame();

        IntStream.range(0, Game.KALAH_PIT_NUMBER).forEach(pitNumber ->
                testGame.getPlayerSide(PLAYER_1).emptyPit(pitNumber));
        //when -> then
        assertThat(GameUtils.areAllPitsEmpty(testGame.getPlayerSide(PLAYER_1))).isTrue();
    }

    @Test
    public void areAllPitsEmpty_pitsAreFull() throws Exception {
        //given
        Game testGame = getStartGame();
        //when -> then
        assertThat(GameUtils.areAllPitsEmpty(testGame.getPlayerSide(PLAYER_1))).isFalse();
    }

}