package pl.simon.kalah.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import pl.simon.kalah.GameUtil;
import pl.simon.kalah.data.GameRepository;
import pl.simon.kalah.logic.GameEngine;
import pl.simon.kalah.model.Game;
import pl.simon.kalah.util.GameException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Szymon on 2017-06-28.
 */
@RunWith(MockitoJUnitRunner.class)
public class GameServiceTest extends GameUtil {

    @Mock
    GameEngine gameEngine;

    @Mock
    GameRepository gameRepository;

    GameService gameService;
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Before
    public void setUp() {
        gameService = new GameService(gameRepository, gameEngine);
    }

    @Test
    public void getGame_noSuchGame() throws Exception {
        //then
        expectedEx.expect(GameException.class);
        expectedEx.expectMessage("No game with name:" + GAME_NAME);

        //given
        when(gameRepository.findOne(GAME_NAME)).thenReturn(null);
        //when
        gameService.getGame(GAME_NAME);

    }

    @Test
    public void validateGameInit_gameWithGivenNameAlreadyExists() throws Exception {
        //then
        expectedEx.expect(GameException.class);
        expectedEx.expectMessage("Game with given name:" + GAME_NAME + " already exists.");

        //given
        when(gameRepository.exists(GAME_NAME)).thenReturn(true);
        //when
        gameService.validateGameInit(GAME_NAME, PLAYER_1, PLAYER_2, 6);
    }

    @Test
    public void validateGameInit_playerNamesAreEqual() throws Exception {
        //then
        expectedEx.expect(GameException.class);
        expectedEx.expectMessage("Players cannot be equal.");

        //when
        gameService.validateGameInit(GAME_NAME, PLAYER_1, PLAYER_1, 6);
    }

    @Test
    public void validateGameInit_stoneNumberIsOutside3to6range() throws Exception {
        //then
        expectedEx.expect(GameException.class);
        expectedEx.expectMessage("Stone number must be from range 3 to 6.");

        //when
        gameService.validateGameInit(GAME_NAME, PLAYER_1, PLAYER_2, 7);
    }

    @Test
    public void validateMove_gameHasAlreadyFinished() throws Exception {
        //then
        expectedEx.expect(GameException.class);
        expectedEx.expectMessage("Game has already finished.");

        //given
        when(gameEngine.isGameFinished(anyObject())).thenReturn(true);
        //when
        gameService.validateMove(getStartGame(), PLAYER_1, 3);

    }

    @Test
    public void validateMove_gameHasAnotherMovingPlayer() throws Exception {
        //given
        String movingPlayer = PLAYER_1;
        Game testGame = getStartGame();
        testGame.setMovingPlayer(PLAYER_2);

        when(gameEngine.isGameFinished(anyObject())).thenReturn(false);
        //then
        expectedEx.expect(GameException.class);
        expectedEx.expectMessage("It is not your turn: " + movingPlayer);

        //when
        gameService.validateMove(testGame, movingPlayer, 3);

    }

    @Test
    public void validateMove_pitNumberIsOutOfRange() throws Exception {
        //given
        String movingPlayer = PLAYER_1;
        Game testGame = getStartGame();
        testGame.setMovingPlayer(PLAYER_1);
        int pitNumber = Game.KALAH_PIT_NUMBER;

        when(gameEngine.isGameFinished(anyObject())).thenReturn(false);
        //then
        expectedEx.expect(GameException.class);
        expectedEx.expectMessage("Pit number is not correct: " + pitNumber);

        //when
        gameService.validateMove(testGame, movingPlayer, pitNumber);

    }

    @Test
    public void validateMove_chosenPitIsEmpty() throws Exception {
        //given
        String movingPlayer = PLAYER_1;
        Game testGame = getStartGame();
        testGame.setMovingPlayer(PLAYER_1);
        int pitNumber = 3;
        testGame.getPlayerSide(PLAYER_1).emptyPit(3);

        when(gameEngine.isGameFinished(anyObject())).thenReturn(false);
        //then
        expectedEx.expect(GameException.class);
        expectedEx.expectMessage("Chosen pit is empty");

        //when
        gameService.validateMove(testGame, movingPlayer, pitNumber);
    }

    @Test
    public void move_checkExecution() throws Exception {
        //given
        String movingPlayer = PLAYER_1;
        Game testGame = getStartGame();
        testGame.setMovingPlayer(PLAYER_1);
        int pitNumber = 3;

        when(gameRepository.findOne(GAME_NAME)).thenReturn(testGame);
        //when
        gameService.move(GAME_NAME, movingPlayer, pitNumber);
        //then

        verify(gameEngine).makeAMove(testGame, pitNumber, movingPlayer);
        verify(gameRepository).save(testGame);
    }

    @Test
    public void init_checkExecution() throws Exception {
        //given
        when(gameRepository.exists(GAME_NAME)).thenReturn(false);
        //when
        gameService.initGame(GAME_NAME, PLAYER_1, PLAYER_2, 6);
        //then
        ArgumentCaptor<Game> argumentCaptor = ArgumentCaptor.forClass(Game.class);
        verify(gameRepository).save(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("stoneNumber", 6)
                .hasFieldOrPropertyWithValue("name", GAME_NAME);
        assertThat(argumentCaptor.getValue().getPlayers()).containsExactly(PLAYER_1, PLAYER_2);
    }


}