package pl.simon.kalah;

import pl.simon.kalah.model.Game;

/**
 * Created by Szymon on 2017-06-28.
 */
public class GameUtil {

    public static final String GAME_NAME = "gameName";
    public static final String PLAYER_1 = "player1";
    public static final String PLAYER_2 = "player2";

    protected static Game getStartGame() {
        Game initialStateGame = new Game(GAME_NAME, PLAYER_1, PLAYER_2, 6);
        return initialStateGame;
    }
}
