package pl.simon.kalah.model;

import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Created by sboh on 6/28/2017.
 */
@NoArgsConstructor
public class GameSide {

    private int[] playerPits;

    GameSide(int stoneNumber){
        playerPits = new int[7];
        initPits(stoneNumber);
    }

    private void initPits(int stoneNumber) {
        IntStream.range(0, Game.KALAH_PIT_NUMBER).forEach(pit -> {
            playerPits[pit] = stoneNumber;
        });
        playerPits[Game.KALAH_PIT_NUMBER] = 0;
    }

    public int getKalah(){
        return playerPits[Game.KALAH_PIT_NUMBER];
    }

    public int getPitValue(int pitNumber){
        return playerPits[pitNumber];
    }

    public void addStonesToPit(int pitNumber, int stonesToAdd){
        playerPits[pitNumber] += stonesToAdd;
    }

    public void addStonesToKalah(int stonesToAdd){
        playerPits[Game.KALAH_PIT_NUMBER] += stonesToAdd;
    }

    public void emptyPit(int pitNumber){
        playerPits[pitNumber] = 0;
    }

    public int[] getOrdinaryPits() {
        return Arrays.copyOfRange(playerPits, 0, Game.KALAH_PIT_NUMBER);
    }
}
