package pl.simon.kalah.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.ImmutableMap;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.Set;

/**
 * Created by Szymon on 2017-06-26.
 */
@NoArgsConstructor
public class Game {

    public static final int KALAH_PIT_NUMBER = 6;
    @Getter
    private Map<String, GameSide> gameBoard;
    @Getter
    private int stoneNumber;
    @Getter @Setter
    private String movingPlayer;
    @Getter
    private String name;
    @Setter
    private Pair<String, Integer> lastAction;

    public Game(String name, String player1, String player2, int stoneNumber) {
        this.name = name;
        gameBoard = ImmutableMap.of(player1, new GameSide(stoneNumber), player2, new GameSide(stoneNumber));
        this.stoneNumber = stoneNumber;
    }

    @JsonIgnore
    public GameSide getPlayerSide(String player){
        return gameBoard.get(player);
    }

    public Set<String> getPlayers(){
        return gameBoard.keySet();
    }

    @JsonIgnore
    public String getLastActionSideOwner(){
        return lastAction.getLeft();
    }

    @JsonIgnore
    public int getLastActionPitNumber(){
        return lastAction.getRight();
    }
}
