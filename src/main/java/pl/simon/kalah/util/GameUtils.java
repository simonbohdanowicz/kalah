package pl.simon.kalah.util;

import org.apache.commons.lang3.RandomUtils;
import pl.simon.kalah.model.GameSide;

import java.util.*;
import java.util.stream.IntStream;

/**
 * Created by Szymon on 2017-06-26.
 */
public class GameUtils {

    public static boolean areAllPitsEmpty(GameSide gameSide){
        return Arrays.stream(gameSide.getOrdinaryPits()).allMatch(score -> score == 0);
    }

    public static String chooseRandomPlayer(List<String> players){
        Collections.shuffle(players);
        return players.get(0);
    }


}
