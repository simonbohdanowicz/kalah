package pl.simon.kalah.util;

/**
 * Created by Szymon on 2017-06-26.
 */
public class GameException extends RuntimeException{

    public GameException(String errorMessage){
        super(errorMessage);
    }
}
