package pl.simon.kalah.logic;

import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Component;
import pl.simon.kalah.model.Game;
import pl.simon.kalah.model.GameSide;
import pl.simon.kalah.util.GameUtils;

import java.util.Arrays;
import java.util.stream.IntStream;

import static pl.simon.kalah.model.Game.KALAH_PIT_NUMBER;

/**
 * Created by sboh on 6/28/2017.
 */
@Component
public class GameEngine {

    public boolean isGameFinished(Game game) {
        return game.getPlayers().stream().map(game::getPlayerSide).allMatch(gameSide -> GameUtils.areAllPitsEmpty(gameSide));
    }

    public void makeAMove(Game game, int pitNumber, String movingPlayer) {
        game.setMovingPlayer(movingPlayer);
        int pitStones = game.getPlayerSide(movingPlayer).getPitValue(pitNumber);
        game.getPlayerSide(movingPlayer).emptyPit(pitNumber);
        moveStonesOnSide(game, movingPlayer, pitStones, pitNumber + 1, false);
        attackIfPossible(game);
        if(isThereNewMovingPlayer(game.getLastActionSideOwner(), game.getLastActionPitNumber(), movingPlayer)){
            game.setMovingPlayer(getOtherPlayer(game, movingPlayer));
        }
        checkIfGameHasEnded(game);
    }

    @VisibleForTesting
    void checkIfGameHasEnded(Game game) {
        game.getPlayers().stream()
                .filter(player -> GameUtils.areAllPitsEmpty(game.getPlayerSide(player)))
                .findAny()
                .ifPresent(player -> {
                    GameSide otherPlayerSide = game.getPlayerSide(getOtherPlayer(game, player));
                    IntStream.range(0, KALAH_PIT_NUMBER).forEach(pitNumber -> {
                                otherPlayerSide.addStonesToKalah(otherPlayerSide.getPitValue(pitNumber));
                                otherPlayerSide.emptyPit(pitNumber);
                            });
                });
    }

    @VisibleForTesting
    boolean isThereNewMovingPlayer(String lastActionSideOwner, int lastActionPitNumber, String currentMovingPlayer) {
        return !lastActionSideOwner.equals(currentMovingPlayer) || lastActionPitNumber != KALAH_PIT_NUMBER;
    }

    @VisibleForTesting
    void attackIfPossible(Game game) {
        int lastPit = game.getLastActionPitNumber();
        String sideOwner = game.getLastActionSideOwner();
        if (isAttackIsPossible(sideOwner, lastPit, game.getPlayerSide(sideOwner).getPitValue(lastPit), game.getMovingPlayer())) {
            int oppositePitNumber = 5 - lastPit;
            int stonesToSteal = game.getPlayerSide(getOtherPlayer(game, sideOwner)).getPitValue(oppositePitNumber);
            game.getPlayerSide(getOtherPlayer(game, sideOwner)).emptyPit(oppositePitNumber);
            game.getPlayerSide(sideOwner).addStonesToKalah(stonesToSteal + 1);
            game.getPlayerSide(sideOwner).emptyPit(lastPit);
        }
    }

    @VisibleForTesting
    boolean isAttackIsPossible(String lastActionSideOwner, int lastActionPitNumber, int lastActionPitStoneNumber, String currentMovingPlayer){
        return lastActionSideOwner.equals(currentMovingPlayer) && lastActionPitNumber != KALAH_PIT_NUMBER && lastActionPitStoneNumber == 1;
    }

    @VisibleForTesting
    void moveStonesOnSide(Game game, String sidePlayer, int stoneNumber, int initialPit, boolean opponent) {
        int range = (stoneNumber + initialPit) >= 5 ? 6 : (stoneNumber + initialPit);
        int leftOver = stoneNumber - (range - initialPit);
        if (!opponent && leftOver > 0) {
            range++;
            leftOver--;
        }
        IntStream moveStream = IntStream.range(initialPit, range);
        moveStream.forEach(pit -> {
            game.getPlayerSide(sidePlayer).addStonesToPit(pit, 1);
            game.setLastAction(Pair.of(sidePlayer, pit));
        });
        if (leftOver > 0) {
            moveStonesOnSide(game, getOtherPlayer(game, sidePlayer), leftOver, 0, !opponent);
        }
    }

    @VisibleForTesting
    String getOtherPlayer(Game game, String player) {
        return game.getPlayers().stream().filter(sidePlayer -> !sidePlayer.equals(player)).findAny().get();
    }

}
