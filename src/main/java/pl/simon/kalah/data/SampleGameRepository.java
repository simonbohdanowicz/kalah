package pl.simon.kalah.data;

import com.google.common.collect.Maps;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.simon.kalah.model.Game;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by Szymon on 2017-06-26.
 */
@Repository
public class SampleGameRepository implements GameRepository {

    private Map<String, Game> gamesRepo = Maps.newConcurrentMap();

    @Override
    public Game save(Game game) {
        return gamesRepo.putIfAbsent(game.getName(), game);
    }

    @Override
    public <S extends Game> Iterable<S> save(Iterable<S> gamesToSave) {
        return gamesToSave;
    }

    @Override
    public Game findOne(String gameName) {
        return gamesRepo.get(gameName);
    }

    @Override
    public boolean exists(String gameName) {
        return gamesRepo.containsKey(gameName);
    }

    @Override
    public Iterable<Game> findAll() {
        return null;
    }

    @Override
    public Iterable<Game> findAll(Iterable<String> gameNames) {
        return null;
    }

    @Override
    public long count() {
        return gamesRepo.size();
    }

    @Override
    public void delete(String gameName) {
        gamesRepo.remove(gameName);
    }

    @Override
    public void delete(Game game) {
        delete(game.getName());
    }

    @Override
    public void delete(Iterable<? extends Game> gamesToDelete) {
        gamesToDelete.forEach(gameToDelete -> delete(gameToDelete.getName()));
    }

    @Override
    public void deleteAll() {
        gamesRepo.clear();
    }
}
