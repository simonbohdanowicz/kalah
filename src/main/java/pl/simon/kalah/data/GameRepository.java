package pl.simon.kalah.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import pl.simon.kalah.model.Game;

import java.io.Serializable;

/**
 * Created by Szymon on 2017-06-26.
 */
public interface GameRepository extends CrudRepository<Game, String> {

}
