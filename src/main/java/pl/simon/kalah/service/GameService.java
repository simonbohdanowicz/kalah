package pl.simon.kalah.service;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.simon.kalah.data.GameRepository;
import pl.simon.kalah.logic.GameEngine;
import pl.simon.kalah.model.Game;
import pl.simon.kalah.util.GameException;
import pl.simon.kalah.util.GameUtils;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * Created by Szymon on 2017-06-26.
 */
@Component
public class GameService {


    private GameRepository gameRepository;
    private GameEngine gameEngine;

    @Autowired
    GameService(GameRepository gameRepository, GameEngine gameEngine) {
        this.gameRepository = gameRepository;
        this.gameEngine = gameEngine;
    }


    public Game getGame(String gameName) {
        Game game = gameRepository.findOne(gameName);
        if(isNull(game)){
            throw new GameException("No game with name:"+gameName);
        }
        return game;
    }

    public void initGame(String gameName, String player1, String player2, int stoneNumber) {
        validateGameInit(gameName, player1, player2, stoneNumber);
        Game game = new Game(gameName, player1, player2, stoneNumber);
        game.setMovingPlayer(GameUtils.chooseRandomPlayer(Lists.newArrayList(player1, player2)));
        gameRepository.save(game);
    }

    public void move(String gameName, String movingPayer, int pitNumber) {
        Game game = getGame(gameName);
        validateMove(game, movingPayer, pitNumber);
        gameEngine.makeAMove(game, pitNumber, movingPayer);
        gameRepository.save(game);
    }

    @VisibleForTesting
    void validateGameInit(String gameName, String player1, String player2, int stoneNumber) {
        if(gameRepository.exists(gameName)){
            throw new GameException("Game with given name:"+gameName+" already exists.");
        }
        if (player1.equals(player2)) {
            throw new GameException("Players cannot be equal.");
        };
        if (stoneNumber < 3 || stoneNumber > 6) {
            throw new GameException("Stone number must be from range 3 to 6.");
        };
    }

    @VisibleForTesting
    void validateMove(Game game, String movingPlayer, int pitNumber) {
        if (gameEngine.isGameFinished(game)) {
            throw new GameException("Game has already finished.");
        };
        if (!game.getMovingPlayer().equals(movingPlayer)) {
            throw new GameException("It is not your turn: " + movingPlayer);
        }
        if (pitNumber < 0 || pitNumber > 5) {
            throw new GameException("Pit number is not correct: " + pitNumber);
        }
        if (game.getPlayerSide(movingPlayer).getPitValue(pitNumber) == 0) {
            throw new GameException("Chosen pit is empty");
        }
    }

}
