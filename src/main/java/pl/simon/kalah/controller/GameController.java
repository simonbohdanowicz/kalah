package pl.simon.kalah.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.simon.kalah.model.Game;
import pl.simon.kalah.service.GameService;

/**
 * Created by Szymon on 2017-06-26.
 */
@RestController
@RequestMapping("/kalah")
public class GameController {

    private final GameService gameService;

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @RequestMapping(method = RequestMethod.POST, produces = {"application/json"})
    ResponseEntity<Game> addNewGame(
            @RequestParam(value = "game-name") String gameName,
            @RequestParam(value = "player1") String player1,
            @RequestParam(value = "player2") String player2,
            @RequestParam(value = "stone-number") int stoneNumber) {
        gameService.initGame(gameName, player1, player2, stoneNumber);
        return ResponseEntity.ok(gameService.getGame(gameName));
    }

    @RequestMapping(path = "/{gameName}", produces = {"application/json"}, method = RequestMethod.GET)
    ResponseEntity<Game> getGame(@PathVariable(value = "gameName") String gameName) {
        return ResponseEntity.ok(gameService.getGame(gameName));
    }

    @RequestMapping(path = "/{gameName}/move", method = RequestMethod.PUT)
    ResponseEntity<Void> makeAMove(
            @PathVariable(value = "gameName") String gameName,
            @RequestParam(value = "player") String player,
            @RequestParam(value = "pit-number") int pitNumber) {
        gameService.move(gameName, player, pitNumber);
        return ResponseEntity.noContent().build();
    }
}
