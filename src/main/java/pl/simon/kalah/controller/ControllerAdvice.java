package pl.simon.kalah.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.simon.kalah.util.GameException;

/**
 * Created by Szymon on 2017-06-28.
 */
@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(GameException.class)
    public ResponseEntity<String> handleNotFoundException(GameException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

}
